#include <Time.h>
#include <TimeLib.h>
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>

////////////////////////////////////////////////////////////////////////

IPAddress ip_arduino(192, 168, 1, 109);     //IP ARDUINO
IPAddress ip_camara(192, 168, 1, 108);      //IP CAMARA
IPAddress ip_client(192, 168, 1, 107);      //IP NOTEBOOK
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress dns(8, 8, 8, 8);
byte mac_eth_Shield[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

unsigned int localPort = 8080;

char packetBuffer_Tipo[1];
char packetBuffer_Duracion[5];
char packetBuffer_Aux[5];
unsigned int Duracion_Ensayo = 65535;
int Tipo_Ensayo = 0;
int Aux_Lectura = 0;
int Cantidad_Ensayos_Disponibles = 12;
//Tipo_Ensayo == 0 --> OF
//Tipo_Ensayo == 1 --> HB E
//Tipo_Ensayo == 2 --> HB T
//Tipo_Ensayo == 3 --> NOR E
//Tipo_Ensayo == 4 --> NOR T
//Tipo_Ensayo == 5 --> FS E
//Tipo_Ensayo == 6 --> FS T
//Tipo_Ensayo == 7 --> Otro E
//Tipo_Ensayo == 8 --> Otro T

int f=0;

int Corriendo_Experimento=0;
int fin_experimento=0;
int cant_agujeros=0;
int PULSADOR=0;
int FILA_01=0;
int FILA_02=0;
int FILA_03=0;
int FILA_04=0;
int COLUMNA_01=0;
int COLUMNA_02=0;
int COLUMNA_03=0;
int COLUMNA_04=0;
unsigned long tiempo_ini = 4294967295;
unsigned long tiempo_actual = 0;
unsigned long dif_tiempo_ensayo = 0;

int EstadoActual_fin_exp=0;
int EstadoActual_pulsador=0;

int EstadoAnterior_fin_exp=0;
int EstadoAnterior_pulsador=0;


int FLANCO_fin_exp=0;
int FLANCO_pulsador=0;

int info[500][2];

int delay_lectura=300;
int delay_egreso=10;

int FLANCO_HOLE[17];
int EstadoActual[17];
int EstadoAnterior[17];
int HOLE[17];

EthernetUDP Udp;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int Lectura_Hole(int Flanco_xx, unsigned long tiempo, int cant_agu, int array_info[500][2], int Hole_xx){
  
      unsigned long tiempo_xx=0;
      unsigned long dif_Tiempo_xx=0;
      
      if (Flanco_xx == 1){
        tiempo_xx = millis();
        dif_Tiempo_xx = (tiempo_xx/1000)-(tiempo/1000); // SE PUEDE AGREGAR UN -2 PARA CORREGIR EL DELAY DE LA CAMARA; PERO EL ENSAYO DURA 2 SEGUNDOS MENOS; VER
        
        Serial.print("Tiempo Agujero ");          //Esto es para verlo en consola, luego no va
        Serial.print(Hole_xx);                    //Esto es para verlo en consola, luego no va
        Serial.print(": ");                       //Esto es para verlo en consola, luego no va
        Serial.print(dif_Tiempo_xx);              //Esto es para verlo en consola, luego no va
        Serial.println(" seg");                   //Esto es para verlo en consola, luego no va
        info[cant_agu][0] = Hole_xx;
        info[cant_agu][1] = dif_Tiempo_xx;
        cant_agu ++;
        }
return cant_agu;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
void udp_setup() 
{
  Ethernet.begin(mac_eth_Shield, ip_arduino);
  Udp.begin(localPort);
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
void udp_check() 
{
  int packetSize = Udp.parsePacket();
  //Serial.println(packetSize);
  if (packetSize){

      Udp.read(packetBuffer_Aux, 5);
      Aux_Lectura = atol(packetBuffer_Aux);

      if (Aux_Lectura > Cantidad_Ensayos_Disponibles){
        Duracion_Ensayo = Aux_Lectura;
        memset(packetBuffer_Aux, 0, sizeof(packetBuffer_Aux));
      }else{
        Tipo_Ensayo = Aux_Lectura;
        memset(packetBuffer_Aux, 0, sizeof(packetBuffer_Aux));
      }
  
      Serial.println(Duracion_Ensayo);
      Serial.println(Tipo_Ensayo);
      Aux_Lectura = 0;
      delay(10);
  }
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
void udp_send(int u)
{
    Udp.beginPacket(ip_client, localPort);
    Udp.print(u);
    Udp.endPacket();   
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
void setup() {
  Serial.begin(9600);
  udp_setup(); 
  pinMode(LED_BUILTIN, OUTPUT);     //LED INTERNO PARA PRUEBAS
  pinMode(31, INPUT);               //FILA_01
  pinMode(33, INPUT);               //FILA_02
  pinMode(35, INPUT);               //FILA_03
  pinMode(37, INPUT);               //FILA_04
  pinMode(39, INPUT);               //COLUMNA_01
  pinMode(41, INPUT);               //COLUMNA_02
  pinMode(43, INPUT);               //COLUMNA_03
  pinMode(45, INPUT);               //COLUMNA_04
  pinMode(47, INPUT);               //PULSADOR
  pinMode(49, OUTPUT);              //LED indicativo lectura 16 agujeros y pulsador
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
void loop() {
  
  FILA_01 = digitalRead(31);
  FILA_02 = digitalRead(33);
  FILA_03 = digitalRead(35);
  FILA_04 = digitalRead(37);
  COLUMNA_01 = digitalRead(39);
  COLUMNA_02 = digitalRead(41);
  COLUMNA_03 = digitalRead(43);
  COLUMNA_04 = digitalRead(45);
  PULSADOR = digitalRead(47);

  HOLE[1]=FILA_01 && COLUMNA_01;
  HOLE[2]=FILA_01 && COLUMNA_02;
  HOLE[3]=FILA_01 && COLUMNA_03;
  HOLE[4]=FILA_01 && COLUMNA_04;
  HOLE[5]=FILA_02 && COLUMNA_01;
  HOLE[6]=FILA_02 && COLUMNA_02;
  HOLE[7]=FILA_02 && COLUMNA_03;
  HOLE[8]=FILA_02 && COLUMNA_04;
  HOLE[9]=FILA_03 && COLUMNA_01;
  HOLE[10]=FILA_03 && COLUMNA_02; 
  HOLE[11]=FILA_03 && COLUMNA_03;
  HOLE[12]=FILA_03 && COLUMNA_04;
  HOLE[13]=FILA_04 && COLUMNA_01;
  HOLE[14]=FILA_04 && COLUMNA_02;
  HOLE[15]=FILA_04 && COLUMNA_03;
  HOLE[16]=FILA_04 && COLUMNA_04;

   udp_check();
   
//////////////////////////////////////////////////////////////////////////////////////
////////////PRENDO LED CON CUALQUIERA DE LOS 16 AGUJEROS O PULSADOR///////////////////

       if (HOLE[1] == 1 || HOLE[2] == 1 || HOLE[3] == 1 || HOLE[4] == 1 || HOLE[5] == 1 || HOLE[6] == 1 || HOLE[7] == 1
       || HOLE[8] == 1 || HOLE[9] == 1 || HOLE[10] == 1 || HOLE[11] == 1 || HOLE[12] == 1 || HOLE[13] == 1 || HOLE[14] == 1
       || HOLE[15] == 1 || HOLE[16] == 1 || PULSADOR == 1) {
       digitalWrite(49, HIGH);
       }else{
       digitalWrite(49, LOW);
       }

//////////////////////////////////////////////////////////////////////////////////////
////////////////LECTURA DE AGUJEROS Y GENERO FLANCO PARA CONTAR///////////////////////
      for (int i=1; i<17; i++){

          EstadoActual[i] = HOLE[i];
          if ((EstadoActual[i] != EstadoAnterior[i]) && EstadoActual[i] == 1){
              FLANCO_HOLE[i] = 1;
              delay(delay_lectura);
          }else{
              FLANCO_HOLE[i] = 0;
              delay(delay_egreso);
          }
          EstadoAnterior[i] = EstadoActual[i];
      }
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


//f=f+1;
//Serial.println(f); 

//////////////////////////////////////////////////////////////////////////////////////
////////////LECTURA DE PULSADOR, GENERO FLANCO e INICIO EXPERIMENTO///////////////////

      EstadoActual_pulsador = (PULSADOR);        //Detecto Flanco
      if ((EstadoActual_pulsador != EstadoAnterior_pulsador) && EstadoActual_pulsador == 1){
          EstadoAnterior_pulsador = EstadoActual_pulsador;
          FLANCO_pulsador =1;
          delay(delay_lectura);
      }else{
          EstadoAnterior_pulsador = EstadoActual_pulsador;
          FLANCO_pulsador =0;
          delay(delay_egreso);
      }
    
    if (FLANCO_pulsador == 1) {
       
       for (int i=0; i<=499; i++){
              info[i][0]=0;
              info[i][1]=0;
              }
       cant_agujeros = 0;
       Corriendo_Experimento = 1;
       udp_send(1111);
       tiempo_ini = millis();
       }
    
    
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////
/////////////////////GUARDO EN ARRAY EL AGUJERO Y EL TIEMPO///////////////////////////

    cant_agujeros = Lectura_Hole(FLANCO_HOLE[1], tiempo_ini, cant_agujeros, info, 1);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[2], tiempo_ini, cant_agujeros, info, 2);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[3], tiempo_ini, cant_agujeros, info, 3);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[4], tiempo_ini, cant_agujeros, info, 4);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[5], tiempo_ini, cant_agujeros, info, 5);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[6], tiempo_ini, cant_agujeros, info, 6);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[7], tiempo_ini, cant_agujeros, info, 7);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[8], tiempo_ini, cant_agujeros, info, 8);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[9], tiempo_ini, cant_agujeros, info, 9);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[10], tiempo_ini, cant_agujeros, info, 10);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[11], tiempo_ini, cant_agujeros, info, 11);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[12], tiempo_ini, cant_agujeros, info, 12);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[13], tiempo_ini, cant_agujeros, info, 13);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[14], tiempo_ini, cant_agujeros, info, 14);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[15], tiempo_ini, cant_agujeros, info, 15);
    cant_agujeros = Lectura_Hole(FLANCO_HOLE[16], tiempo_ini, cant_agujeros, info, 16);
    
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


        

        
       tiempo_actual = millis();                                                                         //Me guardo el tiempo actual para luego restarlo al tiempo inicial
       dif_tiempo_ensayo = (tiempo_actual/1000)-(tiempo_ini/1000);
  
       if ((Corriendo_Experimento == 1) && (Duracion_Ensayo < dif_tiempo_ensayo) && (dif_tiempo_ensayo < (Duracion_Ensayo + 3))){      //Al comparar con el leido de la app, defino el fin del ensayo    
          fin_experimento = 1;
       }

      EstadoActual_fin_exp = (fin_experimento);        //por cada agujero tengo que hacer esto, serian 16 para generar el flanco
      if ((EstadoActual_fin_exp != EstadoAnterior_fin_exp) && EstadoActual_fin_exp == 1){
          EstadoAnterior_fin_exp = EstadoActual_fin_exp;
          FLANCO_fin_exp =1;
      }else{
          EstadoAnterior_fin_exp = EstadoActual_fin_exp;
          FLANCO_fin_exp =0;
      }

       if (FLANCO_fin_exp == 1){

            Corriendo_Experimento = 0;
            if (Tipo_Ensayo == 1 || Tipo_Ensayo == 2){
              udp_send(9999);                                   //Flag para fin de grabacion con excel  
            } else {
              udp_send(8888);                                   //Flag para fin de grabacion sin excel
            }
            
            if (Tipo_Ensayo == 1 || Tipo_Ensayo == 2){          //Si fue un experimento con agujeros mando datos de agujeros
              
                    for (int i=0; i<cant_agujeros; i++){
                    udp_send(info[i][0]);
                    udp_send(info[i][1]);
                    Serial.print(info[i][0]);
                    Serial.print("  ");
                    Serial.println(info[i][1]);
                    }
                    udp_send(7777);
                    for (int i=0; i<=cant_agujeros; i++){   //creo que está de mas
                    info[i][0]=0;                           //creo que está de mas
                    info[i][1]=0;                           //creo que está de mas
                    }                                       //creo que está de mas
                    cant_agujeros = 0;                      //creo que está de mas
                    
            }
      }
   fin_experimento = 0;
   
//////////////////FINAL///////////////////
}
